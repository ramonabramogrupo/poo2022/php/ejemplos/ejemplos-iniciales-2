<?php

$numeros=[];

// añadir elementos a un array

// meter algo al final del array
$numeros[]=23;   // esto entra en la posicion 0

$numeros[]=50; // esto entra en la posicion 1

$numeros[5]=100;  // esto entra en la posicion 5

var_dump($numeros);


$caracteres=[];
// introducir elementos mediante push

// introducir una a
$caracteres[]='a';              // opcion a
array_push($caracteres,'a');    // opcion b

// introducir b y c
// opcion 1
$caracteres[]='b';
$caracteres[]='c';

// opcion 2
array_push($caracteres,'b','c');

var_dump($caracteres);




