<?php

$numero = 22;

// comprobar si el numero es par o impar
// solucion 1
if ($numero % 2 == 0) {
    echo "es par";
} else {
    echo "es impar";
}

// solucion 2
$salida = "Es impar";
if ($numero % 2 == 0) {
    $salida = "Es Par";
}
echo $salida;

//solucion 3
if ($numero % 2 == 0) {
    $salida = "Es Par";
} else {
    $salida = "Es impar";
}
echo $salida;

//solucion 4
if ($numero%2) {
    $salida="Es impar";
}else{
    $salida="Es par";
}

