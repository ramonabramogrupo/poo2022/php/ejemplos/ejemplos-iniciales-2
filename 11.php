<?php

// array es enumerado
$numeros=[
    1,  // $numeros[0]
    45, // $numeros[1]
    8, // $numeros[2]
    7, // $numeros[3]
    ];

$caracteres=[
    "a", // $caracteres[0]
    "j", // $caracteres[1]
    "s", // $caracteres[2]
    "a", // $caracteres[3]
];

var_dump($numeros);
var_dump($caracteres);
