<?php

$datos=[2,6,89]; // array enumerado

$valores=[
    "id" => 1,
    "nombre" => "ana"
]; // array asociativo

// calculando la longitud del array datos
$longitud1=count($datos);

// calculando la longitud del array valores
$longitud2=count($valores);

?>

<?php
// mostrar el primer array,
// datos, en una lista
?>

<ul>
    <li>Longitud: <?= $longitud1 ?></li>
    <li><?= $datos[0] ?></li>
    <li><?= $datos[1] ?></li>
    <li><?= $datos[2] ?></li>
</ul>

<?php
// mostrar el segundo array 
// en una tabla 
?>

<table border="1">
    <tr>
        <td>ID: <?= $valores["id"] ?></td>
    </tr>
    <tr>
        <td>NOMBRE: <?= $valores["nombre"] ?></td>
    </tr>
</table>



