<?php
$nota=7;

// si la nota es menor que 5 suspenso
// si la nota es mayor que 5 aprobado

// solucion 1
$salida="Aprobado";
if($nota<5){
    $salida="Suspenso";
}
echo $salida;

// solucion 2
if($nota<5){
    $salida="Suspenso";
}else{
    $salida="Aprobado";
}
echo $salida;

// solucion 3
if($nota>=5){
    $salida="Aprobado";
}else{
    $salida="Suspenso";
}
echo $salida;

// solucion 4
$salida=["suspenso","aprobado"];

$posicion=round($nota/10);
        
echo $salida[$posicion];




